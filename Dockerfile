FROM node:14-alpine

WORKDIR /usr/src/app

# Instalando todas sus dependencias
COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000
CMD [ "npm", "start" ]