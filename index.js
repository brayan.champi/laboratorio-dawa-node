import express from 'express'
import { mockup } from './mockup'
const app = express()
let schedule = mockup

app.use(express.json())

app.get('/', (request, response) => {
  response.send('<h1>Hello World!</h1>')
})

app.get('/api/persons', (request, response) => {
  response.json(schedule)
})

app.get('/info', (request, response) => {
  const countPersons = 'Phonebook has info for ' + schedule.length + ' people'
  const hoy = new Date()
  response.send(countPersons + '</br>' + hoy)
})

app.get('/api/persons/:id', (request, response) => {
  const id = parseInt(request.params.id)
  const person = schedule.find(person => {
    return person.id === id
  })
  if (person) {
    response.json(person)
  } else {
    response.status(500).send('Persona no encontrada')
  }
})

app.delete('/api/persons/:id', (request, response) => {
  const id = parseInt(request.params.id)
  schedule = schedule.filter(schedule => schedule.id !== id)
  response.status(200).send('Persona borrada')
})

app.post('/api/persons', (request, response) => {
  if (!request.params.name && !request.params.number) {
    return response.status(400).send('Falta el nombre y teléfono')
  }

  const person = schedule.find(person => {
    return person.id === request.params.id
  })

  if (person === request.params.name) {
    return response.status(400).send('El nombre de la persona ya existe')
  }

  const persons = {
    id: getRandomArbitrary(10, 100),
    name: request.params.id,
    number: request.params.number
  }
  response.json(persons)
})

app.use(function (req, res, next) {
  res.status(404).send('No se encontró la dirección ingresada!')
})

function getRandomArbitrary (min, max) {
  return Math.random() * (max - min) + min
}

const PORT = process.env.PORT || 3000
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`)
})
